// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function (sisa1) {
    readBooks(sisa1, books[1], function (sisa2) {
        readBooks(sisa2, books[2], function (sisa3) {
            readBooks(sisa3, books[3], function () {
                console.log("semua buku sudah dibaca")
            })
        })
    })
})



// const callback = (waktu) => {
//     i++
//     readBooks(waktu, books[i], callback)
// }

// var i = 0;
// // books.forEach((item) => {
// readBooks(10000, books[i], callback)
// // })

// books.forEach(function (item) {
//     readBooks(10000, item, callback)
// })

// const posts = [
//     {
//         title: "Post one",
//         body: "This is post one"
//     },
//     {
//         title: "Post two",
//         body: "This is post two"
//     }
// ]

// const createPost = (post, callback) => {
//     setTimeout(() => {
//         posts.push(post)
//         callback()
//     }, 2000)
// }

// const getPosts = () => {
//     setTimeout(() => {
//         posts.forEach(post => {
//             console.log(post)
//         })
//     }, 1000)
// }

// const newPost = {
//     title: "Post three",
//     body: "This is post three"
// }

// createPost(newPost, getPosts)
// getPosts()


// const posts = [
//     {
//         title: "Post one",
//         body: "This is post one"
//     },
//     {
//         title: "Post two",
//         body: "This is post two"
//     }
// ]

// const createPost = post => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             posts.push(post)
//             const error = false
//             if (!error) {
//                 resolve()
//             } else {
//                 reject()
//             }
//         }, 2000)
//     })
// }

// const getPosts = () => {
//     setTimeout(() => {
//         posts.forEach(post => {
//             console.log(post)
//         })
//     }, 1000)
// }

// const newPost = {
//     title: "Post three",
//     body: "This is post three"
// }

// createPost(newPost)
//     .then(getPosts)
//     .catch(error => console.log(error))

// const posts = [
//     {
//         title: "Post one",
//         body: "This is post one"
//     },
//     {
//         title: "Post two",
//         body: "This is post two"
//     }
// ]

// const createPost = post => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             posts.push(post)
//             const error = false
//             if (!error) {
//                 resolve()
//             } else {
//                 reject()
//             }
//         }, 2000)
//     })
// }

// const getPosts = () => {
//     setTimeout(() => {
//         posts.forEach(post => {
//             console.log(post)
//         })
//     }, 1000)
// }

// const newPost = {
//     title: "Post three",
//     body: "This is post three"
// }

// const init = async () => {
//     await createPost(newPost)
//     getPosts()
// }

// init()